# DevJam.org Podcast Directory project (podcast-directory)

A podcast directory is a web application where users' can search and discover podcast shows. Users can easily find podcast episodes and information.

## Install the dependencies
```bash
yarn
# or
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```


### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.config.js](https://v2.quasar.dev/quasar-cli-webpack/quasar-config-js).
