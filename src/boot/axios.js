import { boot } from 'quasar/wrappers'
import axios from 'axios'
import sha1 from 'sha1'

// Be careful when using SSR for cross-request state pollution
// due to creating a Singleton instance here;
// If any client changes this (global) instance, it might be a
// good idea to move this instance creation inside of the
// "export default () => {}" function below (which runs individually
// for each client)
let api = null
const apiKey = process.env.PODCAST_INDEX_API
const apiSecret = process.env.PODCAST_INDEX_SECRET

export default boot(({ app }) => {
    const time = Math.floor(+Date.now() / 1000)
    const hash = sha1(`${apiKey}${apiSecret}${time}`)

    api = axios.create({
        baseURL: 'https://api.podcastindex.org/api/1.0',
        headers: {
            'X-Auth-Key': apiKey,
            'X-Auth-Date': time,
            Authorization: hash,
        },
    })
})

export { api, apiKey, apiSecret }
