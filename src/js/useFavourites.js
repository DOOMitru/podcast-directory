import { api } from 'src/boot/axios'
import { ref } from 'vue'
const favourites = ref([])

export default function useFavourites() {
    const toggleFavourite = feed => {
        let found = favourites.value.find(f => f.id === feed.id)

        if (found) {
            favourites.value = favourites.value.filter(f => f.id !== feed.id)
        } else favourites.value = [...favourites.value, feed]

        localStorage.setItem('favourites', JSON.stringify(favourites.value))
    }

    const loadFavourites = () => {
        let favs = localStorage.getItem('favourites')
        favourites.value = favs === null ? [] : JSON.parse(favs)
    }

    const getFavIcon = feed => {
        return favourites.value.find(f => f.id === feed.id) ? 'favorite' : 'favorite_border'
    }

    const openLink = async link => {
        try {
            let response = await api.get(`/podcasts/byfeedid?id=${link.id}`)
            window.open(response.data.feed.link, '_blank')
        } catch (error) {
            console.log(error)
        }
    }

    return {
        favourites,
        toggleFavourite,
        loadFavourites,
        getFavIcon,
        openLink,
    }
}
